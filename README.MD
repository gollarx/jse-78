# TASK MANAGER

## DEVELOPER INFO

* **Name**: Alexey Shipilov

* **E-mail**: gollarx@gmail.com

* **E-mail**: ashipilov@t1-consulting.ru

## SOFTWARE

* **OS**: Windows 10 (Build 17763)

* **JAVA**: OpenJDK 1.8.0_345

## HARDWARE

* **CPU**: i5

* **RAM**: 16GB

* **SSD**: 256GB

## PROGRAM BUILD

```shell
mvn clean install
```

## PROGRAM RUN

```shell
java -jar ./task-manager.jar
```