package ru.t1.shipilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.dto.model.ProjectDTO;

public interface IProjectService extends IUserOwnedService<ProjectDTO> {

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @NotNull
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @NotNull Status status);

    @Nullable
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    ProjectDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
