package ru.t1.shipilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.api.repository.IRepository;
import ru.t1.shipilov.tm.enumerated.EntitySort;
import ru.t1.shipilov.tm.dto.model.AbstractModelDTO;

import java.util.List;

public interface IService<M extends AbstractModelDTO> extends IRepository<M> {

    @NotNull
    List<M> findAll(@Nullable EntitySort entitySort);

}
