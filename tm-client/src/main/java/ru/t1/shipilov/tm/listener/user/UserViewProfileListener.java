package ru.t1.shipilov.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shipilov.tm.dto.request.UserProfileRequest;
import ru.t1.shipilov.tm.dto.model.UserDTO;
import ru.t1.shipilov.tm.event.ConsoleEvent;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    private final String NAME = "view-user-profile";

    @NotNull
    private final String DESCRIPTION = "View profile of current user.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@userViewProfileListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @Nullable final UserDTO user = authEndpoint.profile(request).getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
