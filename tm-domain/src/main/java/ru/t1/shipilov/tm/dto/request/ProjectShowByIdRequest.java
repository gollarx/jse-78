package ru.t1.shipilov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectShowByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectShowByIdRequest(@Nullable final String token) {
        super(token);
    }

}
