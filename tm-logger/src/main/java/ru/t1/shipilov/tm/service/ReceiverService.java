package ru.t1.shipilov.tm.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.shipilov.tm.api.IReceiverService;

import javax.jms.*;

@Service
@AllArgsConstructor
public final class ReceiverService implements IReceiverService {

    @NotNull
    private static final String QUEUE = "LOGGER";

    @NotNull
    @Autowired
    private final ConnectionFactory connectionFactory;

    @Override
    @SneakyThrows
    public void receive(@NotNull final MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createQueue(QUEUE);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}
