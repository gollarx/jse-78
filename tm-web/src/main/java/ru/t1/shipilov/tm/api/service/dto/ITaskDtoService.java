package ru.t1.shipilov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.dto.TaskDTO;

import java.util.Collection;
import java.util.List;

public interface ITaskDtoService {

    void save(@Nullable final String userId, @Nullable final TaskDTO task);

    void saveAll(@Nullable final String userId, @Nullable final Collection<TaskDTO> tasks);

    void removeAll(@Nullable final String userId);

    void removeAll(@Nullable final String userId, @Nullable Collection<TaskDTO> tasks);

    void removeOneById(@Nullable final String userId, @Nullable final String id);

    void removeOne(@Nullable final String userId, @Nullable final TaskDTO task);

    @NotNull
    List<TaskDTO> findAll(@Nullable final String userId);

    @Nullable
    TaskDTO findOneById(@Nullable final String userId, @Nullable final String id);

}
