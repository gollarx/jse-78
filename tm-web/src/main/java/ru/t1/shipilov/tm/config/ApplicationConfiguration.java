package ru.t1.shipilov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import ru.t1.shipilov.tm.endpoint.ProjectCollectionSoapEndpoint;
import ru.t1.shipilov.tm.endpoint.ProjectSoapEndpoint;
import ru.t1.shipilov.tm.endpoint.TaskCollectionSoapEndpoint;
import ru.t1.shipilov.tm.endpoint.TaskSoapEndpoint;

@Configuration
@EnableWs
public class ApplicationConfiguration extends WsConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(servlet, "/ws/*");
    }

    @Bean(name = "ProjectEndpoint")
    public DefaultWsdl11Definition projectWsdl11Definition(@NotNull final XsdSchema projectEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(ProjectSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(ProjectSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(ProjectSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(projectEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "ProjectCollectionEndpoint")
    public DefaultWsdl11Definition projectCollectionWsdl11Definition(@NotNull final XsdSchema projectCollectionEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(ProjectCollectionSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(ProjectCollectionSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(ProjectCollectionSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(projectCollectionEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "TaskEndpoint")
    public DefaultWsdl11Definition taskWsdl11Definition(@NotNull final XsdSchema taskEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TaskSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(TaskSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(TaskSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(taskEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "TaskCollectionEndpoint")
    public DefaultWsdl11Definition taskCollectionWsdl11Definition(@NotNull final XsdSchema taskCollectionEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TaskCollectionSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(TaskCollectionSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(TaskCollectionSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(taskCollectionEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema projectEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectEndpoint.xsd"));
    }

    @Bean
    public XsdSchema projectCollectionEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectCollectionEndpoint.xsd"));
    }

    @Bean
    public XsdSchema taskEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/taskEndpoint.xsd"));
    }

    @Bean
    public XsdSchema taskCollectionEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/taskCollectionEndpoint.xsd"));
    }

}
