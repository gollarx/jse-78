package ru.t1.shipilov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CredentialsDTO {

    protected String username;

    protected String password;

}
