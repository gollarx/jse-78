package ru.t1.shipilov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Result {

    private Boolean success = true;

    private String message = "";

    public Result(String message) {
        this.message = message;
    }

    public Result(Boolean success) {
        this.success = success;
    }

    public Result(Exception e) {
        success = false;
        message = e.getMessage();
    }

}
