package ru.t1.shipilov.tm.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "app_user")
@Getter
@Setter
public class UserDTO {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String login;

    @Column(name = "password_hash")
    private String passwordHash;

}
