package ru.t1.shipilov.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.shipilov.tm.dto.TaskDTO;

import javax.xml.bind.annotation.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "tasksSaveRequest")
public class TasksSaveRequest {

    @XmlElement(required = true)
    protected List<TaskDTO> tasks;

}
