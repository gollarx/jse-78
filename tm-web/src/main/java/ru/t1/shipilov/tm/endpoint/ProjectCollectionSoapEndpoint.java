package ru.t1.shipilov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.shipilov.tm.api.service.dto.IProjectDtoService;
import ru.t1.shipilov.tm.api.service.model.IProjectService;
import ru.t1.shipilov.tm.dto.soap.*;
import ru.t1.shipilov.tm.model.CustomUser;

@Endpoint
public class ProjectCollectionSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "ProjectCollectionSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.shipilov.t1.ru/dto/soap";


    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectsFindAllRequest", namespace = NAMESPACE)
    public ProjectsFindAllResponse findCollection(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final ProjectsFindAllRequest request) {
        return new ProjectsFindAllResponse(projectDtoService.findAll(user.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectsSaveRequest", namespace = NAMESPACE)
    public ProjectsSaveResponse saveCollection(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final ProjectsSaveRequest request) {
        projectDtoService.saveAll(user.getUserId(), request.getProjects());
        return new ProjectsSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectsUpdateRequest", namespace = NAMESPACE)
    public ProjectsUpdateResponse updateCollection(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final ProjectsUpdateRequest request) {
        projectDtoService.saveAll(user.getUserId(), request.getProjects());
        return new ProjectsUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectsDeleteRequest", namespace = NAMESPACE)
    public ProjectsDeleteResponse deleteCollection(
            @AuthenticationPrincipal final CustomUser user,
            @RequestPayload final ProjectsDeleteRequest request) {
        projectService.removeAll(user.getUserId(), request.getProjects());
        return new ProjectsDeleteResponse();
    }

}
