<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../include/_header.jsp"/>

<form name="f" action="/auth" method="POST">
    <table width="400" border="1" cellpadding="10" cellspacing="0" align="center">
        <tr>
            <td colspan="2">
                <h3>Login with Username and Password</h3>
            </td>
        </tr>
        <tr>
            <td>User:</td>
            <td><input type="text" name="username" value=""/></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type="password" name="password"/></td>
        </tr>
        <tr>
            <td colspan="2"><input name="submit" type="submit" value="Login"/></td>
        </tr>
    </table>

</form>

<jsp:include page="../include/_footer.jsp"/>